const express = require('express');
//create an application using express
//This creates an express application and stores this as a constant called app.
//app is our server
const app = express()
//for our application server to run, we need a port to listen to
const port = 3000;

//Setup for allowing the server to handle data from requests
//Allows your app to read json data
//Methods used from express JS are middlewares

app.use(express.json());
//allows your app to read data from forums
//By default, information received from the url can only be received as a string or an array
//By applying the option of "extended: true" this allows us to receive information in other data
//such as an object/boolean etc, which we will use throughout our application
//Middleware is software the provides services outside of what's offered by the operationg system
app.use(express.urlencoded({ extended:true }))

//We will create routes
//Express has methods corressponding to each HTTP method

app.get('/users', (req, res) => {
	//res.send uses the express JS module's method instead to send a response
	//back to the client
	res.send(users)
})

app.get('/home', (req, res ) => {
	res.send("Welcome to the Home Page!")
})

/*app.post('/hello', (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})*/

let users = [];

app.post('/signup', (req, res) => {
	//if contents of the request body with the property "username" and "password" is not empty
	if(req.body.username !== '' && req.body.password !== ''){
		//This will store the user objectt sent via POSTMANN to the users array created above
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered!`)
	}else {
		res.send("Please input BOTH username and password")
	}})

//update the password of a user that matches the information providedd in the client /Postman
app.delete('/delete-user', (req,res) => {
	let message;
	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			//Changes the password of the user found by the loop into the password provided inn the client /postman
			req.body.username == users.splice(i)

			//Changes the message to be sent back by the response

			message = `User ${req.body.username} has been deleted`
			//Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;
		}else {
			message = `User does not exist`
		}
	}
	res.send(message)
})



app.listen(port, () => console.log(`Server is running at port ${port}`));



